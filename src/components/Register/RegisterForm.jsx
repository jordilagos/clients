import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './RegisterForm.scss';

import { register } from '../../api/authentication';
import { login } from '../../api/login';

class RegisterForm extends Component {
  state = {
    form: {
      username: '',
      phone: 0,
      email: '',
      password: '',
    },
    error: null,
  };

  handleSubmit = async (ev) => {
    ev.preventDefault();

    try {
      const data = await register(this.state.form);
      if (data !== 'Failed to fetch') {
        console.log('Registro completado');
      } else {
        this.setState({
          error: 'No se ha podido completar la operación',
        });
      }
    } catch (error) {
      console.log(error.message);
      this.setState({
        error: error.message,
      });
    }
  };
  handleSubmitLogin = async (ev) => {
    ev.preventDefault();
    try {
      const data = await login(this.state.form);
      if (data !== 'Failed to fetch') {
        console.log('Login correcto');
      } else {
        this.setState({
          error: 'No se ha podido completar la operación',
          email: '',
          password: '',
        });
      }
    } catch (error) {
      console.log(error.message);
      this.setState({
        error: error.message,
      });
    }
  };
  handleOnChangeInput = (ev) => {
    const { name, value } = ev.target;
    this.setState((prevState) => ({
      form: {
        ...prevState.form,
        [name]: value,
      },
    }));
  };
  render() {
    const isLogin = this.props.isLogin;
    const { form, error } = this.state;
    const { username, phone, email, password } = form;
    const isDisable = !email || !password;
    return isLogin ? (
      <form onSubmit={this.handleSubmit} className="RegisterForm__form">
        <label htmlFor="email" className="RegisterForm__label">
          <p>Correo electrónico</p>
          <input
            type="email"
            name="email"
            value={email}
            onChange={this.handleOnChangeInput}
          ></input>
        </label>
        <label htmlFor="password" className="RegisterForm__label">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            value={password}
            onChange={this.handleOnChangeInput}
          ></input>
        </label>

        {error ? <p style={{ color: 'red' }}>{error}</p> : null}
        <button
          type="submit"
          className="RegisterForm__submit-button"
          disabled={isDisable}
        >
          Iniciar sesión
        </button>
      </form>
    ) : (
      <form onSubmit={this.handleSubmitLogin} className="RegisterForm__form">
        <label htmlFor="username" className="RegisterForm__label">
          <p>Nombre</p>
          <input
            type="text"
            name="username"
            value={username}
            onChange={this.handleOnChangeInput}
          ></input>
        </label>
        <label htmlFor="phone" className="RegisterForm__label">
          <p>Teléfono</p>
          <input
            type="number"
            name="phone"
            value={phone}
            onChange={this.handleOnChangeInput}
          ></input>
        </label>
        <label htmlFor="email" className="RegisterForm__label">
          <p>Correo electrónico</p>
          <input
            type="email"
            name="email"
            value={email}
            onChange={this.handleOnChangeInput}
          ></input>
        </label>
        <label htmlFor="password" className="RegisterForm__label">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            value={password}
            onChange={this.handleOnChangeInput}
          ></input>
        </label>

        {error ? <p style={{ color: 'red' }}>{error}</p> : null}
        <button
          type="submit"
          className="RegisterForm__submit-button"
          disabled={isDisable}
        >
          Registrarme
        </button>
      </form>
    );

  }
}

RegisterForm.propTypes = {
  isLogin: PropTypes.bool,
};

export default RegisterForm;
