import React, { Component } from 'react';

import RegisterForm from '../Register';

import './Home.scss';

class Home extends Component {
      state = {
    isLogin: false,
  };
  handleOnClickLogin = () => {
    this.setState({
      isLogin: true,
    });
  };

  handleOnClickRegister = () => {
    this.setState({
      isLogin: false,
    });
  };
  render() {
    
    return (
      <div className="Home__nav">
        {this.state.isLogin ? (
          <button
            className="Home__nav-button"
            onClick={this.handleOnClickRegister}
          >
            Necesito registrarme
          </button>
        ) : (
          <button
            className="Home__nav-button"
            onClick={this.handleOnClickLogin}
          >
            Ya estoy registrado, puedo iniciar sesión
          </button>
        )}

        {this.state.isLogin ? (
          <RegisterForm isLogin={this.state.isLogin} />
        ) : (
          <RegisterForm isLogin={this.state.isLogin} />
        )}
      </div>
    );
  }
}

export default Home;
