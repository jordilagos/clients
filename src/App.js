import React from 'react';

import Home from './components/Home';

import './App.scss';

class App extends React.Component {
  render() {
    return <div className="App">
      <h1>Welcome to my App</h1>
      <Home /> 
    </div>;
  }
}

export default App;
