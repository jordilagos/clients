const baseUrl='https://examen.avirato.com/';
const loginUrl = `${baseUrl}client/get`;
const modifyUrl= `${baseUrl}client/post`;
const getAllUrl = `${baseUrl}client/get/search`;

/**
 *
 * @param {Object} userData
 * @param {string} userData.username
 * @param {string} userData.email
 * @param {number} userData.phone
 * @param {string} userData.password
 */

export const login = async (userData) => {
  try {
    const response = await fetch(loginUrl, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify(userData),
    });
    const jsonResponse = await response.json();
    if (!response.ok) {
      throw new Error(jsonResponse.message);
    }

    return jsonResponse.data;
  } catch (err) {
    return err.message;
  }
};

export const modify = async (userData) => {
    try {
        const response = await fetch(modifyUrl, {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(userData),
        });
        const jsonResponse = await response.json();
        if (!response.ok) {
          throw new Error(jsonResponse.message);
        }
    } catch (err) {
        return err.message;
      }
};

export const logout = async (userData) => {};

export const getAll = async () => {
    try {
        const response = await fetch(getAllUrl, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          credentials: 'include',
          body: JSON.stringify(),
        });
        const jsonResponse = await response.json();
        if (!response.ok) {
          throw new Error(jsonResponse.message);
        }
        return jsonResponse.data;

    } catch (err) {
        return err.message;
      }
};
