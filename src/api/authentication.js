const registerUrl = 'https://examen.avirato.com/auth/login';

/**
 *
 * @param {Object} userData
 * @param {string} userData.email
  * @param {string} userData.password
 */

export const register = async (userData) => {
  try {
    const response = await fetch(registerUrl, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify(userData),
    });
    const jsonResponse = await response.json();

    if (!response.ok) {
      throw new Error(jsonResponse.message);
    }

   
    return jsonResponse.data;
  } catch (err) {
    return err.message;
  }
};
